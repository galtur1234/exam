// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDP6M66e5Ppqqfen7SquZ-HRuFe9Kx1fSk",
    authDomain: "exam-ab857.firebaseapp.com",
    projectId: "exam-ab857",
    storageBucket: "exam-ab857.appspot.com",
    messagingSenderId: "784328481114",
    appId: "1:784328481114:web:4b9f7927b907124005fb63"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
