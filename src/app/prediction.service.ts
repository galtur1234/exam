import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

url = 'https://46sbrz6cr8.execute-api.us-east-1.amazonaws.com/beta'

predict(gradeMath: number, gradePsych: number, tuition:boolean):Observable<any>{
  let json = {
    "data": 
      {
        "gradeMath": gradeMath,
        "gradePsych": gradePsych,
        "tuition":tuition
      }
  }
  let body  = JSON.stringify(json);
  return this.http.post<any>(this.url,body).pipe(
    map(res => {
      return res.body;       
    })
  );      
}

constructor(private http: HttpClient) { }

}
