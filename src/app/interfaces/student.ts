export interface Student {
    id:string,
    gradeMath: number,
    gradePsych: number,
    tuition:boolean,
    prob?:string,
    pressSave?:number
}
