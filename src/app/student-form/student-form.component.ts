import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Student } from '../interfaces/student';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  constructor() { }

  @Input() gradeMath:number;
  @Input() gradePsych:number;
  @Input() tuition:boolean;
  @Input() id:string;
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
  @Input() formType:string;

  updateParent(){
    let student:Student = {id:this.id, gradeMath:this.gradeMath, gradePsych:this.gradePsych,tuition:this.tuition };
    this.update.emit(student);
    if(this.formType == "Add student"){
      this.gradeMath  = null;
      this.gradePsych = null; 
      this.tuition = null; 

    }
 
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  onSubmit(){
    
  }

  ngOnInit(): void {
  }




}
