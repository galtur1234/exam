import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('students'); 

  students = [{gradeMath:90, gradePsych:700,tuition:true}, {gradeMath:70, gradePsych:600,tuition:true}];


  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`);
    return this.studentCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }

  deleteStudent(Userid:string, id:string){
    this.db.doc(`users/${Userid}/students/${id}`).delete(); 
  } 


  public addStudents() {
    setInterval(()=>this.students.push({gradeMath:700, gradePsych:800, tuition:true}),2000);
  }




  updateStudent(userId:string,id:string,gradeMath:number,gradePsych:number, tuition:boolean){
    this.db.doc(`users/${userId}/students/${id}`).update(
      {
        gradeMath:gradeMath,
        gradePsych:gradePsych,
        tuition:tuition
      }
    )
  }

  save(userId:string,id:string,gradeMath:number,gradePsych:number,tuition:boolean,prob:string,pressSave:number){
    this.db.doc(`users/${userId}/students/${id}`).update(
      {
        gradeMath:gradeMath,
        gradePsych:gradePsych,
        tuition:tuition,
        prob:prob,
        pressSave:pressSave
      }
    )
  }


  addStudent(userId:string,gradeMath:number,gradePsych:number,tuition:boolean ){
    const student = {gradeMath:gradeMath, gradePsych:gradePsych,tuition:tuition }; 
    this.userCollection.doc(userId).collection('students').add(student);
  }


  constructor(private db:AngularFirestore) { }

}
