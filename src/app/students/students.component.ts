import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';
import { AuthService } from './../auth.service';
import { PredictionService } from './../prediction.service';

@Component({
  selector: 'students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  panelOpenStat = false;
  //students;
  students$;
  userId:string;
  favoriteTuition: boolean;
  tuitions: boolean[] = [true, false];
  editstate = []; 
  addStudentFormOpen = false;
  
  constructor(private studentsService:StudentsService, public authService:AuthService, private predictionService:PredictionService) { }

  save(students:Student){
    this.studentsService.save(this.userId ,students.id ,students.gradeMath, students.gradePsych, students.tuition ,students.prob,students.pressSave=2);
  }

  add(student:Student){
    this.studentsService.addStudent(this.userId,student.gradeMath,student.gradePsych, student.tuition); 
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.students$ = this.studentsService.getStudents(this.userId);
      }
    )
}

deleteStudent(id:string){
  this.studentsService.deleteStudent(this.userId,id); 
}

public predict(student:Student){
  this.predictionService.predict(student.gradeMath,student.gradePsych, student.tuition).subscribe(
    res => {
      console.log(res);
      if(res > 0.5){
        console.log('Does not fall off');
        student.prob='Does not fall off';
        student.pressSave=1;
      } else {
        console.log('fall off');
        student.prob='fall off';
        student.pressSave=1;
      }
    }
  )
}


update(student:Student){
  this.studentsService.updateStudent(this.userId,student.id ,student.gradeMath, student.gradePsych, student.tuition);
}

  

}
